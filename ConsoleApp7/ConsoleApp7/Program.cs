﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Binar bin = new Binar();
            Console.WriteLine(bin.Get("001100"));
            Console.ReadKey();
        }
    }

    public class Binar
    {
        public string Get(string n)
        {
            int s = 0;
            int j = 1;
            for (int i = 0; i < n.Length; i++, j *= 2)
                if (n[i] == '1')
                    s = s + j;
            return Convert.ToString(s);
        }
    }
}
