using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp7;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string s = "00111100";
            string ea = "60";
            Binar bin = new Binar();
            string ra = bin.Get(s);

            Assert.AreEqual(ea, ra);
        }

        [TestMethod]
        public void TestMethod2()
        {
            string s = "0";
            string ea = "0";
            Binar bin = new Binar();
            string ra = bin.Get(s);

            Assert.AreEqual(ea, ra);
        }

        [TestMethod]
        public void TestMethod3()
        {
            string s = "11111111";
            string ea = "255";
            Binar bin = new Binar();
            string ra = bin.Get(s);

            Assert.AreEqual(ea, ra);
        }
    }
}
